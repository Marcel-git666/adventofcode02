import Foundation

func printLine() -> String {
    let file = "/Users/marcel/Documents/Programming/submarine.txt" //this is the file. we will write to and read from it
    var text: String
    do {
        text = try String(contentsOfFile: file)
        
        }
    catch {
        print("Chyba souboru.")
        return(error.localizedDescription)
        /* error handling here */}
    return text
}



var str : String
var i : Int
var count=0
str=printLine()
print(partOne())
print(partTwo())

func partOne() -> String {
    var x = 0
    var y = 0

    str.split(separator: "\n").forEach { line in
        let operation = line.split(separator: " ").first
        let number = Int(line.split(separator: " ").last!)!

        switch operation {
            case "forward": x+=number
            case "up": y-=number
            case "down": y+=number
            default: print("Unexpected line")
        }
    }
    return(x*y).description
}


func partTwo() -> String {
    var x = 0
    var aim = 0
    var depth = 0

    str.split(separator: "\n").forEach { line in
        let operation = line.split(separator: " ").first
        let number = Int(line.split(separator: " ").last!)!

        switch operation {
            case "forward":
                x+=number
                depth=depth+aim*number
                
            case "up": aim-=number
            case "down": aim+=number
            default: print("Unexpected line")
        }
    }
    return(x*depth).description
}
